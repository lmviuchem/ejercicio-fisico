#Aquí se creará la interfaz de nuestro programa

import pygame
import sys

pygame.init()

screen = pygame.display.set_mode((500,500))
pygame.display.set_caption("Planos inclinados")

#Variables del triángulo
colorTriangulo = (144, 12, 63)

#Variables de los objetos con masa
colorCuadrado = (52, 152, 219)
posXCua = 410
posYCua = 310

colorCirculo = (52, 152, 219)
posXCir = 100
posYCir = 328
#Variables de las cuerdas
colorCuerda = (255, 255, 255)

#Variables de la polea
colorPolea = (218, 247, 166)

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
            
   
    screen.fill((0,0,0))
    linea1 = pygame.draw.aaline(screen, colorCuerda, (100, 328), (400, 178))
    linea2 = pygame.draw.aaline(screen, colorCuerda, (430, 310), (430, 180))
    triangulo = pygame.draw.polygon(screen, colorTriangulo, [[100, 350], [400, 350], [400, 200]], 0)
    circulo = pygame.draw.circle(screen, colorCirculo, (posXCir, posYCir), 20)
    cuadrado = pygame.draw.rect(screen, colorCuadrado, [posXCua, posYCua, 40, 40], 0)
    polea = pygame.draw.circle(screen, colorPolea, (420, 170), 20)
    

    
    pygame.display.update()